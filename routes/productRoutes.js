//! [SECTION] Dependencies
const express = require("express");
const router = express.Router();

//![SECTION] Imported Modules
const productControllers = require("../controllers/productControllers");

//!add Product
const auth = require("../auth");
// it will apply the functions verify, verifyadmin from auth.js.

const { verify, verifyAdmin } = auth; // deconstructor

//! Create/Add Products
router.post("/", verify, verifyAdmin, productControllers.addProduct);

//!Get all Products
router.get("/", productControllers.getAllProducts);

//! get single product
//use id to locate the a single product you want to update
router.get("/getSingleProduct/:id", productControllers.getSingleProduct);

//!update Product by Admin
router.put("/:id", verify, verifyAdmin, productControllers.editProductAdmin);

//! Archive Product by Admin
router.put(
    "/archive/:id",
    verify,
    verifyAdmin,
    productControllers.archivedProductAdmin
);

//! Activate Product
router.put(
    "/activate/:id",
    verify,
    verifyAdmin,
    productControllers.activateProduct
);

//!view all active Products
router.get("/viewAllActiveProducts", productControllers.viewAllActiveProducts);

module.exports = router;