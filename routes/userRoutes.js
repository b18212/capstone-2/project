//! [SECTION] dependencies
const express = require("express");
//http method
const router = express.Router();

const auth = require("../auth");

//* for verification Users/Admin
const { verify } = auth;
const { verifyAdmin } = auth;

//![SECTION] Imported Modules
const userControllers = require("../controllers/userControllers");

//* Check if registered users
router.post("/registerUser", userControllers.registerUser);

//*view all users
router.get("/", userControllers.getAllUsers);

//*login user
router.post("/login", userControllers.loginUser);

//* Check Email if Exists
// router.post("/checkEmail", userControllers.checkEmail);

//* Find user by Email
// router.post("/findUserByEmail", userControllers.findUserByEmail);

//* Update user Details
router.put("/updateUserDetails", verify, userControllers.updateUserDetails);

//*Update user to Admin
router.put(
    "/updateUserAdmin/:id",
    verify,
    verifyAdmin,
    userControllers.updateAdmin
);

//* order products
router.post("/order", verify, userControllers.order);

//* view client order with total
router.get("/getSingleClient /:id", userControllers.getSingleClient);

//* view all orders per user
router.get("/getUserOrder", verify, userControllers.getUserOrder);

//* view all orders by Admin Only
router.get(
    "/viewAllOrdersAdmin",
    verify,
    verifyAdmin,
    userControllers.viewAllOrdersAdmin
);

module.exports = router;