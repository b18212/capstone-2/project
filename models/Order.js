const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
    totalAmount: {
        type: Number,
        required: [true, "Total Amount is required"],
    },

    purchasedDate: {
        type: Date,
        default: new Date(),
    },

    orderStatus: {
        type: String,
        default: [true, "Ordered"],
    },
});

module.exports = mongoose.model("Order", orderSchema);