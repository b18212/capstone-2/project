const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Product Name is required"],
    },

    description: {
        type: String,
        required: [true, "Product description is required"],
    },

    price: {
        type: Number,
        required: [true, "Product price is required"],
    },

    isActive: {
        type: Boolean,
        default: true,
    },

    createdOn: {
        type: Date,
        default: new Date(),
    },

    customers: [{
        userId: {
            type: String,
            required: [true, "User Id is required"],
        },

        // productPrice: {
        //     type: Integer,
        //     required: [true, "Price is required."],
        // },

        dateOrdered: {
            type: Date,
            default: new Date(),
        },

        status: {
            type: String,
            default: "Ordered",
        },
    }, ],
});

module.exports = mongoose.model("Product", productSchema);