//! Dependencies
//relative path, model creation

//! Modules
const User = require("../models/User");
//hashing of password
const Product = require("../models/Product");
const bcrypt = require("bcryptjs");
//authentication
const auth = require("../auth");

//! [SECTION] Register User
//POST: User Register
module.exports.registerUser = (req, res) => {
    console.log(req.body); //assurance where you will get the data

    //* password hashing, use hashSync method

    const hashedPW = bcrypt.hashSync(req.body.password, 10);

    //* Create a new user document out of our user model
    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPW,
    });

    newUser
        .save()
        .then((user) => res.send(user))
        .catch((err) => res.send(err));

    // User.findOne({ email: req.body.email })
    //     .then((foundEmail) => {
    //         if (foundEmail === null) {
    //             return newUser.save(), res.send("Successfully registered.");
    //         } else {
    //             return res.send("Email is already registered.");
    //         }
    //     })
    //     .catch((err) => res.send(err));
};

//! [SECTION] Retrieval/ Get All users/View

module.exports.getAllUsers = (req, res) => {
    User.find({})
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//! [SECTION] Login User

module.exports.loginUser = (req, res) => {
    console.log(req.body);

    User.findOne({ email: req.body.email })
        .then((foundUser) => {
            if (foundUser === null) {
                return res.send("No user found in the database.");
            } else {
                //bcrypt method, compareSync. uses 2 value that you want to compare. Boolean result
                const isPasswordCorrect = bcrypt.compareSync(
                    req.body.password,
                    foundUser.password
                );
                console.log(isPasswordCorrect);

                //* compareSync()
                // will return a boolean value so if it matches, this will return true if not, this will return false

                //no need comparison because of truthy values
                if (isPasswordCorrect) {
                    return res.send({ accessToken: auth.createAccessToken(foundUser) });
                } else {
                    return res.send("Incorrect password please try again.");
                }
            }
        })
        .catch((err) => res.send(err));
};

//! [SECTION] Updating User Details

module.exports.updateUserDetails = (req, res) => {
    console.log(req.body); // input for new values
    console.log(req.user.id); //if user is already logged in
    let updates = {
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        mobileNo: req.body.mobileNo,
        email: req.body.email,
    };

    //new:true reflect the updated data
    //where to get updates, id, options
    //findById so you will not go through params
    User.findByIdAndUpdate(req.user.id, updates, { new: true })
        .then((updatedUser) => res.send(updatedUser))
        .catch((err) => res.send(err));
};

//![SECTION] User Update to admin

module.exports.updateAdmin = (req, res) => {
    console.log(req.user.id); //id of the logged in user
    console.log(req.params.id); //this will be the id of the user we want to update

    let updates = {
        isAdmin: true,
    };

    //! inside where to update
    User.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then((updatedUser) => res.send(updatedUser))
        .catch((err) => res.send(err));
};

//! [SECTION] Order Products here
module.exports.order = async(req, res) => {
    console.log(req.user.id);
    console.log(req.body.productId);
    console.log(req);

    if (req.user.isAdmin) {
        return res.send("Action Forbidden");
    }

    let isUserUpdated = await User.findById(req.user.id).then((user) => {
        let newPurchase = {
            productId: req.body.productId,
            productPrice: req.body.productPrice,
        };
        user.productOrders.push(newPurchase);
        user.totalAmount.push(newPurchase.productPrice);

        console.log(user.productOrders);
        let sum = user.totalAmount.reduce((prev, current) => prev + current);

        User.findByIdAndUpdate(req.user.id, { sumOrders: sum }, { new: true })
            .then((user) => user)
            .catch((err) => err.message);
        // let isUserUpdated = await User.findById(req.user.id).then((user) => {
        //     console.log(user);

        //     let newOrder = {
        //         productId: req.body.productId,
        //     };

        //     user.productOrders.push(newOrder);
        return user
            .save()
            .then((user) => true)
            .catch((err) => err.message);
    });

    if (isUserUpdated !== true) {
        return res.send({ message: isUserUpdated });
    }

    let isProductUpdated = await Product.findById(req.body.productId).then(
        (product) => {
            let customer = {
                userId: req.user.id,
            };

            product.customers.push(customer);

            return product
                .save()
                .then((product) => true)
                .catch((err) => err.messaqge);
        }
    );

    if (isProductUpdated !== true) {
        //false
        return res.send({ message: isProductUpdated });
    }

    if (isUserUpdated && isProductUpdated) {
        return res.send({ message: "User ordered successfully!" });
    }
};

//![SECTION] View Per Client with Order and Total
module.exports.getSingleClient = (req, res) => {
    Product.findById(req.params.id)
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//! [SECTION] View All Orders Per User
//need to find out who ordered
module.exports.getUserOrder = (req, res) => {
    //result (all details)

    User.findById(req.user.id) //to check if log in

    .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//!View all users by Admin Only
module.exports.viewAllOrdersAdmin = (req, res) => {
    let arr = [];
    Product.find({})
        .then((result) => {
            result.forEach((item) => {
                console.log(item);
                item.customers.length !== 0 &&
                    arr.push({
                        name: item.name,
                        description: item.description,
                        price: item.price,
                        customers: item.customers,
                    });
            });
            console.log("------------ARR-----------");
            console.log(arr);

            res.send({ arr });
        })
        .catch((err) => res.send(err));
};

//! [SECTION] View Single User's Order

module.exports.getUserOrders = (req, res) => {
    User.findById(req.user.id)
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};