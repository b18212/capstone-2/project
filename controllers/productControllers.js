//! Dependencies
const Product = require("../models/Product");

//! [SECTION] Add Products (Admin Only)
module.exports.addProduct = (req, res) => {
    console.log(req.body); //assurance where you will get the data

    let newProduct = new Product({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
    });

    newProduct
        .save()
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//! [SECTION] View All Products
module.exports.getAllProducts = (req, res) => {
    console.log(req.body);

    Product.find({})
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//! [SECTION] View Single Products
module.exports.getSingleProduct = (req, res) => {
    Product.findById(req.params.id)
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//! [SECTION] Update Product (Admin Only)
module.exports.editProductAdmin = (req, res) => {
    let updateAdmin = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
    };

    Product.findByIdAndUpdate(req.params.id, updateAdmin, { new: true })
        .then((updatedProduct) => res.send(updatedProduct))
        .catch((err) => res.send(err));
};

//! [SECTION] Update Archive/Inactive Product by Admin Only
module.exports.archivedProductAdmin = (req, res) => {
    let archiveAdmin = {
        isActive: false,
    };

    Product.findByIdAndUpdate(req.params.id, archiveAdmin, { new: true })
        .then((archivedProductAdmin) => res.send(archivedProductAdmin))
        .catch((err) => res.send(err));
};

//![SECTION] for activate Product
module.exports.activateProduct = (req, res) => {
    let activate = {
        isActive: true,
    };

    Product.findByIdAndUpdate(req.params.id, activate, { new: true })
        .then((activatedProduct) => res.send(activatedProduct))
        .catch((err) => res.send(err));
};

//![SECTION] View Active Products
module.exports.viewAllActiveProducts = (req, res) => {
    Product.find({ isActive: true })
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};