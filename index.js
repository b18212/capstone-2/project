// ! Required Modules
const express = require("express");
const mongoose = require("mongoose");

//!Port
// const port = 4000;

//! Server
const app = express();
const port = process.env.PORT || 4000;

//! Middleware
app.use(express.json());

//! Mongoose connection (URI, Options)
mongoose.connect(
    "mongodb+srv://admin:admin@cluster0.fzfit.mongodb.net/capstone2?retryWrites=true&w=majority", {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }
);

//(optional) checking if you are connected to MongoDB
const db = mongoose.connection;
db.on("error", console.error.bind(console, "Database Error"));
db.once("open", () => console.log("Successfully connected to MongoDB"));

//!Group routing for users
//from userRoutes.js
const userRoutes = require("./routes/userRoutes");
app.use("/users", userRoutes);

//!Group routing for products
//from productRoutes.js
const productRoutes = require("./routes/productRoutes");
app.use("/product", productRoutes);

//!Port Listener
app.listen(port, () => console.log(`Listening to port: ${port}`));