//![SECTION] DEPENDENCIES
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const User = require("./models/User");
const secret = "HahnCraftAPI";
module.exports.createAccessToken = (user) => {
    console.log(user);

    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin,
    };
    console.log(data);
    return jwt.sign(data, secret, {});
};

//? Goal: to check if the token exist is not tampered
//*verify - for access token
module.exports.verify = (req, res, next) => {
    //req.headers.authorization - contains jwt/ token
    let token = req.headers.authorization;

    if (typeof token === "undefined") {
        //* typeof result is a string

        return res.send({ auth: "Failed. No Token" });
    } else {
        console.log(token);
        //* slice method is used for arrays and strings
        // Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOTZmZWExMmU0NTc0NWI4NjE3ODAyMSIsImVtYWlsIjoiY2FsbE1lTGlzYUBnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjU0MTMxODQyfQ.RVmGn5gzvC1LChXKomeXSsiW0rFqZNDQSmEmvN2_C4o

        token = token.slice(7, token.length);
        console.log(token);
        //* Expected Output
        // eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOTZmZWExMmU0NTc0NWI4NjE3ODAyMSIsImVtYWlsIjoiY2FsbE1lTGlzYUBnbWFpbC5jb20iLCJpc0FkbWluIjpmYWxzZSwiaWF0IjoxNjU0MTMxODQyfQ.RVmGn5gzvC1LChXKomeXSsiW0rFqZNDQSmEmvN2_C4o

        jwt.verify(token, secret, (err, decodedToken) => {
            if (err) {
                return res.send({
                    auth: "Failed",
                    message: err.message,
                });
            } else {
                console.log(decodedToken);

                req.user = decodedToken;

                //* will let us proceed to the next middleware or controller
                next();
            }
        });
    }
};

//! verifying an admin

module.exports.verifyAdmin = (req, res, next) => {
    if (req.user.isAdmin) {
        next();
    } else {
        return res.send({
            auth: "Failed",
            message: "Action Forbidden",
        });
    }
};